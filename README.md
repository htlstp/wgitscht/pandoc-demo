# Docker image for presentation workflow
image is available in dockerhub wgitscht/pandoc:v1 can be built here in case of changes to pandoc/js

## local usage 
to build your presentation locally, in the folder where your presentation resides issue the
following command

```bash
docker run --rm -v "$(pwd):/data" -u $(id -u):$(id -g) pandoc-gits:v1  -t revealjs -s -F mermaid-filter -o /data/index.html /data/slides.md
```

## pipeline
to use it in a pipeline you need to add the .gitalb-ci.yml, if your repo is not
public you also need to change the settings in settings -> pages -> visibility

## different format
also possible to export in ppt, beamer and many more.

## find the result here
[demo](https://htlstp.gitlab.io/wgitscht/pandoc-demo/)
