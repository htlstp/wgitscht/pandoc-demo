---
title: A Demo Presentation
author: GITS
---
# Übersicht2

## Einführung
- wie kann ich eine Präsentation Strukturieren
- wieso ist ein Workflow wichtig

## Pipelines, tools und so
- nur noch ein md file
- jederzeit änderbar
- jederzeit abrufbar
- viele Formate!
 
# Tools
## Techstack
- markdown
- pandoc
- mermaid
- that's it

## Mermaid example
```{.mermaid loc=inline theme=dark background=transparent format=svg}
graph TD;  
  super-->gut;
  super-->supergut;
  gut-->besser;
  besser-->super
```
